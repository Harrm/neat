cmake_minimum_required(VERSION 3.23)
project(NeatEngine)

set(CMAKE_CXX_STANDARD 20)

add_executable(NeatEngine main.cpp engine/application/include/application.hpp engine/core/include/macro/macro.hpp engine/core/include/tracing/tracing.hpp engine/renderer/include/renderer.hpp)
