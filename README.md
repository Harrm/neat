# Manifesto
**NEAT** means:
 - Clean Code
 - Maximal Adequate Performance 
 - Flexibility
 - Fully Automated Building Process
 - Cross-Platform
 - Open-Source
 - Static Analysis and Sanitizers
