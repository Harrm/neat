//
// Created by Harrm on 02.09.2022.
//

#pragma once

namespace neat::application {

    class Application {
    public:
        virtual ~Application() = 0;

        virtual int run(int arc, char** argv) = 0;
    };

}
