//
// Created by Harrm on 02.09.2022.
//

#pragma once

namespace neat::renderer {

    class Renderer {
    public:
        virtual ~Renderer() = default;

        virtual void run() = 0;
    };

}
